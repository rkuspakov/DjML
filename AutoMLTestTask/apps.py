from django.apps import AppConfig


class AutomltesttaskConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AutoMLTestTask'
