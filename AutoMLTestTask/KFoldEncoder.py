from sklearn.model_selection import KFold
import numpy as np
import pandas as pd

class KFoldTargetEncoder():
    def __init__(self, n_fold=5):
        self.n_fold = n_fold
        self.means = pd.Series()

    def fit_transform_train(self,train, colNames,targetName):

        X = train.copy()
        mean_of_target = X[targetName].mean()
        kf = KFold(n_splits = self.n_fold, shuffle = False)
        means_dict = {}

        for colName in colNames:
            col_mean_name = colName + '_enc'
            X[col_mean_name] = np.nan
            for tr_ind, val_ind in kf.split(X):
                X_tr, X_val = X.iloc[tr_ind], X.iloc[val_ind]
                X.loc[X.index[val_ind], col_mean_name] = X_val[colName].map(X_tr.groupby(colName)[targetName].mean())
                X[col_mean_name].fillna(mean_of_target, inplace = True)
                means_dict[colName] =  X[[colName,col_mean_name]].groupby(colName).mean().reset_index()
        self.means = pd.Series(means_dict)
        return X.drop(colNames, axis =1)

    def transform_test(self, test, colNames, means=pd.Series()):
        """закодируем значения признаков на test-выборке средними значениями закодированных признаков train-выборки"""

        if means.empty:
            if not self.means.empty:
                means = self.means
            else:
                raise Exception("Обучите енкодер на обучающей выборке или передайте средние значения параметром")

        X = test.copy()

        for colName in colNames:
            encodedName = colName + '_enc'
            mean =  means[colName]
            dd = {}

            for index, row in mean.iterrows():
                dd[row[colName]] = row[encodedName]

            X[encodedName] = X[colName].apply(lambda x: x if x in dd else None)
            X = X.replace({encodedName: dd}).drop(colName, axis =1)
            X[encodedName].fillna(np.median(list(dd.values())),inplace=True)
        return X
