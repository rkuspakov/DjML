from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect

import pickle
import pandas as pd
from .KFoldEncoder import KFoldTargetEncoder

# Create your views here.

def redir(request):
    return redirect('aml/')

def index(request):
    context = {}
    return render(request, 'index.html', context)

def predict(request):
    if request.method == 'POST':
        file = request.FILES['file_upload']
        handle_uploaded_file(file)
    file = f"AutoMLTestTask/upload/TEST_DATA.csv"
    test_df = pd.read_csv(file, index_col='row_id')

    means = get_means()
    model = get_model()
    cat_cols = means.index

    targetc = KFoldTargetEncoder(n_fold=5)
    new_test_df = targetc.transform_test(test_df, cat_cols, means)

    prediction = pd.Series(model.predict(new_test_df), name="prediction", index=new_test_df.index)

    return HttpResponse(
        prediction.to_csv(),
        content_type="text/csv",
        headers={"Content-disposition":
                     "attachment; filename=filename.csv"})

def handle_uploaded_file(f):
    with open(f"AutoMLTestTask/upload/{f.name}", "wb+") as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def get_means():
    with open('AutoMLTestTask/data/KFoldEncoder_means.pkl', 'rb') as fp:
        means = pickle.load(fp)
    return means


def get_model():
    with open('AutoMLTestTask/data/lgb_model.pkl', 'rb') as mdl:
        model = pickle.load(mdl)
    return model

